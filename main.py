import colorama
import os
import requests as r


try:
    from delta2_command_center.moduleexs_ import *
    from delta2_command_center.modules_ import module_names
except ImportError:
    #module_paths = "./modules_/"
    from modules_ import *
    from modules_ import module_names # list of module names in the modules_ directory
from requests.exceptions import ConnectionError
try:
    from delta2_command_center.utils.module import Modules, Class_utils, list_modules_in_folder

except ImportError:
    from utils.module import Modules, Class_utils, list_modules_in_folder

import cmd
from cmd2 import style
import importlib.util

def string_to_module(module_name, code_string):
    spec = importlib.util.spec_from_loader(module_name, loader=None)
    module = importlib.util.module_from_spec(spec)
    exec(code_string, module.__dict__)
    return module


class SHELL(cmd.Cmd):
    completekey = 'tab'
    API_URL = "http://127.0.0.1:9000/"
    try:
        intro = style(r.get(API_URL).text)
    except ConnectionError:
        print(f"check or configure where the API is configured!")
    
    prompt = f"{colorama.Fore.RED}delta2>{colorama.Fore.RESET} "
    selected = None

    def do_list(self, args):
        """
        List available modules.
        """
        print("Available modules:")
        for module_name in module_names:
            print(f"- {module_name}")

    def do_api(self, args):
        """
        Usage: api http://api_location:apiport/
        """
        if len(args) > 0:
            arg = args.split()[0]
            try:
                self.intro = style(r.get(arg).text)
                print(self.intro)
                print('API configured successfully!')
                self.API_URL = arg
            except ConnectionError:
                print(f'Invalid api url: {arg}')
        else:
            print("Usage: api http://api_location:apiport/")

    def do_quit(self, args):
        return True

    def do_exit(self, args):
        return True
    
    def do_module(self, args):
        """ Select a module """
        print(args)
        if len(args) > 1:
            module_name = args.split()[0]
            if module_name == "list":
                for module_name in module_names:
                    print(f"- {module_name}")
            elif module_name in module_names:
                
                for mod in all_mods:
                    if mod.__name__.replace("modules_.", '') == module_name:
                        module = mod
                class_utils = Class_utils(module)
                self.mod_name = module_name
                self.classes = class_utils.get_classes_with_run()
                self.prompt = f'{colorama.Fore.RED}delta2 ({module_name}) >{colorama.Fore.RESET} '
                self.selected = module
            else:
                print(f'{colorama.Fore.LIGHTRED_EX}Invalid module or command: {module_name}{colorama.Fore.RESET}')


        None
    def do_list_classes(self, args):
        """ List classes inside the module """
        if self.selected != None:
            class_utils = Class_utils(self.selected)
            classes = class_utils.get_classes_with_run()
            print(f"{colorama.Fore.YELLOW}Available classes in {self.selected.__file__}:")
            for cls in classes:
                print(f'- {cls}')
            print(colorama.Fore.RESET)
        else:
            print(f"{colorama.Fore.LIGHTRED_EX}No module has been selected yet.{colorama.Fore.RESET}")


    def do_class(self, args):
        """ Select a class to use """
        if self.selected and len(args) > 1:
            arg = args.split()[0]
            if arg in self.classes:
                self.selected_class = self.selected.__dict__[arg]
                self.selected_class = self.selected_class()
                self.selected_class.options['API_URL'] = self.API_URL
                self.prompt = f'{colorama.Fore.RED}delta2 ({self.mod_name}.{arg}) >{colorama.Fore.RESET} '
            


    def do_show_options(self, args):
        """ Show options for the selected class """
        if self.selected_class:
            class_obj = self.selected_class
            
            print(f"Options for {self.mod_name}.{class_obj.__class__.__name__}:")
            for key, value in class_obj.options.items():
                print(f"- {key}: {value}")
            print(f'{colorama.Fore.YELLOW}read API documentation for more info on options')
            print(f'{colorama.Fore.RESET}')




    def do_set(self, args):
        """ Set arguments for the selected class """
        if self.selected != None:
            if self.selected_class:
                options = self.selected_class.options
                arg = args.split()
                if len(arg) == 2:
                    option = arg[0]
                    value = arg[1]
                    if option in options:
                        print(f"{colorama.Fore.YELLOW}Setting {option} to {value}{colorama.Fore.RESET}")
                        options[option] = value
                    else:
                        print(f"{colorama.Fore.LIGHTRED_EX}Invalid option: {option}{colorama.Fore.RESET}")
                else:
                    print(f"{colorama.Fore.LIGHTRED_EX}Usage: set <option> <value>{colorama.Fore.RESET}")

    def do_run(self, args):
        """ Run the selected class """
        if self.selected != None:
            if self.selected_class:
                class_obj = self.selected_class
                class_obj.run()
            else:
                print(f"{colorama.Fore.LIGHTRED_EX}No class has been selected yet.{colorama.Fore.RESET}")
        else:
            print(f"{colorama.Fore.LIGHTRED_EX}No module has been selected yet.{colorama.Fore.RESET}")
    
    def do_info(self, args):
        if self.selected != None:
            if self.selected_class:
                try:
                    info = self.selected_class.info
                    print(f"{colorama.Fore.YELLOW}Information about {self.mod_name}.{self.selected_class.__name__}:")
                    print(info)
                    print(colorama.Fore.RESET)
                except AttributeError:
                    print(f"{colorama.Fore.LIGHTRED_EX}No information available for {self.mod_name}.{self.selected_class.__name__}{colorama.Fore.RESET}")













if __name__ == "__main__":
    SHELL().cmdloop()













