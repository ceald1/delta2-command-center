import os
import glob
import importlib

# Get the current directory
current_dir = os.path.dirname(os.path.abspath(__file__))

# Get all Python files (modules) in the current directory
module_files = glob.glob(os.path.join(current_dir, '*.py'))

# Extract module names from file paths
module_names = [os.path.basename(file)[:-3] for file in module_files if os.path.basename(file) != '__init__.py']


# Get the current directory path
dir_path = os.path.dirname(os.path.realpath(__file__))

# Get a list of all files in the directory
files = [f for f in os.listdir(dir_path) if f.endswith(".py") and not f.startswith("__")]


all_mods = []
# Import each module dynamically
for file in files:
    module_name = file[:-3]  # Remove the '.py' extension
    module = importlib.import_module(f".{module_name}", package=__name__)
    globals()[module_name] = module  # Make the module accessible in the subpackage namespace
    all_mods.append(module)

