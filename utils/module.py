from pydantic import BaseModel, Field
import os
import inspect
import importlib.util
import builtins

class Modules:
    def __init__(self, class_):
        self.class_ = class_
    
    def set_option(self, option, value):
        """ Set options for the module """
        self.class_.options[option] = value
    
    def run():
        self.class_.run()
    
    def get_info(self):
        return self.class_.info




class Class_utils:
    def __init__(self, module):
        self.module_name = module

    def get_classes_with_run(self):
        classes = []
        for name, obj in inspect.getmembers(self.module_name):
            if inspect.isclass(obj) and hasattr(obj, 'run'):
                classes.append(name)

        return classes


def list_modules_in_folder(folder):
    modules_list = []
    for file in os.listdir(folder):
        if file.endswith(".py") and not file.startswith("__"):
            module_name = file[:-3]  # Remove the .py extension
            modules_list.append(module_name)
    return modules_list

# class Module:
#     def __init__(self, options:dict, class_):
#         self.options = options
#         self.class_ = class_
    

#     def set_option(option):
#         self.class_.options = 

    

#     def run():
#         self.class_.run()
    

#     def print_options(self):
#         for key, value in self.options.items():
#             print(f"{key}: {value}")




# class List_modules:
#     def __init__(self, module_directory="./delta2_command_center/modules_/"):
#         self.modules = module_directory
#         self.module_list = []
#         self.class_list = []
    
#     def list_modules(self):
#         for filename in os.listdir(self.modules):
#             if filename.endswith(".py") and not filename.startswith("__"):
#                 module_name = os.path.splitext(filename)[0]
#                 self.module_list.append(module_name)
    
#     def get_classes(self):
#         for module_name in self.module_list:
#             pass

