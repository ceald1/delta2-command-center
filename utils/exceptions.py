



class OptionNotSet(Exception):
    """
    Exception raised when a required option is not set.
    """
    def __init__(self, option):
        self.message = f'option: {option} is not set and required'
        super().__init__(option)


