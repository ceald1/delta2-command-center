# Command Center

the command center is a CLI tool for interacting with the Delta2 API, it also supports custom modules





## Custom Modules
to write custom modules you need to make your script into a class with a `run` method and an `options` variable, a variable named `info` is not required but is highly recommended. EX:
```python
class Myclass:
    def __init__(self):
        self.options = {"op1":"value",
            "op2":"value", "API_URL": "http://api_url:api_port/"}
        self.info = "example class" # Not required but recommended
    

    def run(self):
        run_some_code()
```
**Note:** you can have multiple classes in one file and it is encouraged!
In your options variable make sure to have a key inside the dictionary called `"API_URL"`. This will be set to the `API_URL` variable inside the `main.py` file when the class inside the Module is selected.
